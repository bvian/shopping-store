const productsAPI = 'https://fakestoreapi.com/products';

const products = document.querySelector('#products');

function createProduct(product) {

    const productDiv = document.createElement('div');
    productDiv.className = "product";

    const productImage = document.createElement('img');
    productImage.className = 'productImage';
    productImage.src = product.image;
    productImage.alt = 'Product Image';

    const productRating = document.createElement('div');
    productRating.className = 'productRating';

    const rating = document.createElement('div');
    rating.className = 'rating';

    const rate = document.createElement('span');
    rate.className = 'rate';

    const rateImage = document.createElement('img');
    rateImage.className = 'icon';
    rateImage.src = 'images/star.png';
    rateImage.alt = 'star';

    const count = document.createElement('span');
    count.className = 'count';

    const countImage = document.createElement('img');
    countImage.className = 'icon';
    countImage.src = 'images/user.png';
    countImage.alt = 'user';

    const description = document.createElement('div');
    description.className = "description";

    const productTitle = document.createElement('h5');
    const price = document.createElement('h4');

    const cartTag = document.createElement('a');
    cartTag.href = "#";

    const cartIcon = document.createElement('i');
    cartIcon.classList.add('fal');
    cartIcon.classList.add('fa-shopping-cart');
    cartIcon.classList.add('cart');

    const priceContainer = document.createElement('div');
    priceContainer.className = 'priceContainer';

    rate.appendChild(rateImage);
    rate.appendChild(document.createTextNode(product.rating.rate));


    count.appendChild(countImage);
    count.appendChild(document.createTextNode(product.rating.count));

    rating.appendChild(rate);
    rating.appendChild(count);

    productTitle.appendChild(document.createTextNode(product.title));

    price.appendChild(document.createTextNode(`$${product.price}`));

    productRating.appendChild(rating);
    productDiv.appendChild(productImage);

    description.appendChild(productRating);
    description.appendChild(productTitle);

    productDiv.appendChild(description);

    cartTag.appendChild(cartIcon);

    priceContainer.appendChild(price);
    priceContainer.appendChild(cartTag);

    productDiv.appendChild(priceContainer);

    return productDiv;
}

fetch(productsAPI)
    .then((products) => {
        return products.json();
    })
    .then((productsData) => {
        const loader = document.querySelector('.loaderLogo');
        loader.style.display = "none";

        if (productsData.length === 0) {
            console.log('No Products!');
        } else {
            const productsHeader = document.createElement('h2');
            productsHeader.innerText = 'All Products';

            const productContainer = document.createElement('div');
            productContainer.className = 'productContainer';

            products.appendChild(productsHeader);
            products.appendChild(productContainer);

            const allProducts = productsData.map((product) => {
                return createProduct(product);
            });

            productContainer.append(...allProducts);
        }
    })
    .catch((err) => {

        products.style.display = 'none';

        const loader = document.querySelector('.loaderLogo');
        loader.style.display = 'none';

        const errorContainer = document.querySelector('.error-container');
        errorContainer.style.display = 'flex';

        const errorImage = document.querySelector('.error-img');
        errorImage.style.display = 'block';

        const footer = document.querySelector('footer');
        footer.style.position = 'fixed';

        console.error(err);
    });


// Menu Bar

const bar = document.getElementById('bar');
const navbar = document.getElementById('navbar');

if (bar) {
    bar.addEventListener('click', () => {
        if (navbar.classList.contains('active')) {
            navbar.classList.remove('active');
        } else {
            navbar.classList.add('active');
        }
    });
} else {
    navbar.style.display = flex;
}

// showing username if user logged in.

function fetchUser() {
    const user = JSON.parse(localStorage.getItem('user'));

    const username = user? user.firstname : undefined;

    const signUpElement = document.getElementById('signup');
    const loginElement = document.getElementById('login');
    const signUpElement1 = document.getElementById('signup1');
    const loginElement1 = document.getElementById('login1');

    if (username !== undefined) {
        signUpElement.href = "/";
        signUpElement.innerText = username;

        signUpElement1.href = "/";
        signUpElement1.innerText = username;

        loginElement.href = "/";
        loginElement.innerText = 'Logout';

        loginElement1.href = "/";
        loginElement1.innerText = 'Logout';
    } else{
        signUpElement.href = "signup.html";
        signUpElement.innerText = "Sign Up";

        signUpElement1.href = "signup.html";
        signUpElement1.innerText = "Sign Up";

        loginElement.href = "";
        loginElement.innerText = 'Login';

        loginElement1.href = "";
        loginElement1.innerText = 'Login';
    }
}

fetchUser();

// logout handler

const login = document.getElementById('login');
const login1 = document.getElementById('login1');

login.addEventListener('click', () => {
    localStorage.clear();
});

login1.addEventListener('click', () => {
    localStorage.clear();
});