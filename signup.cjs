const form = document.getElementById('form');

const firstName = document.getElementById('firstName');
const lastName = document.getElementById('lastName');
const email = document.getElementById('email');
const password = document.getElementById('password');
const confirmPassword = document.getElementById('confirmPassword');
const tosBox = document.getElementById('tosBox');

const loader = document.querySelector('.loaderLogo');
loader.style.display = "none";

// adding user

function addUser() {
    const newUser = {
        email: email.value,
        firstname: firstName.value,
        lastname: lastName.value
    };

    localStorage.setItem("user", JSON.stringify(newUser));
}

function setSuccessMessage(inputElement) {
    if (inputElement.nextElementSibling?.className === 'message') {
        inputElement.nextElementSibling.remove();
        inputElement.style.border = "2px solid #088178";
    } else if (inputElement.nextElementSibling?.nextElementSibling?.className === 'message') {
        inputElement.nextElementSibling.nextElementSibling.remove();
        inputElement.style.border = "2px solid #088178";
    }
    else {
        inputElement.style.border = "2px solid #088178";
    }
}

function setErrorMessage(inputElement, errorMessage) {
    let messageElement;

    if (errorMessage.includes('box')) {

        if (inputElement.nextElementSibling.nextElementSibling?.className !== 'message') {
            messageElement = document.createElement('p');
            messageElement.classList.add('message');
            inputElement.nextElementSibling.after(messageElement);
        } else {
            messageElement = inputElement.nextElementSibling.nextElementSibling;
        }

    } else {

        if (inputElement.nextElementSibling?.className !== 'message') {
            messageElement = document.createElement('p');
            messageElement.classList.add('message');
            inputElement.after(messageElement);
        } else {
            messageElement = inputElement.nextElementSibling;
        }
    }

    messageElement.textContent = errorMessage;
    inputElement.style.border = "2px solid red";
}

function validatingSignUpForm() {

    const nameRegex = /^[a-zA-Z]{2,30}$/;
    const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    let hasError = false;

    if (firstName.value === "") {
        setErrorMessage(firstName, "First name field cannot be blank.");
        hasError = true;
    } else if (!nameRegex.test(firstName.value)) {
        setErrorMessage(firstName, "Please enter valid first name");
        hasError = true;
    } else {
        setSuccessMessage(firstName);
    }

    if (lastName.value === "") {
        setErrorMessage(lastName, "Last name field can't be blank.");
        hasError = true;
    } else if (!nameRegex.test(lastName.value)) {
        setErrorMessage(lastName, "Please enter valid last name");
        hasError = true;
    } else {
        setSuccessMessage(lastName);
    }

    if (email.value === "") {
        setErrorMessage(email, "Email field can't be blank.");
        hasError = true;
    } else if (!emailRegex.test(email.value)) {
        setErrorMessage(email, "Please enter valid email address");
        hasError = true;
    } else {
        setSuccessMessage(email);
    }

    if (password.value === "") {
        setErrorMessage(password, "Password field can't be blank.");
        hasError = true;
    } else if (password.value.length < 8) {
        setErrorMessage(password, "Password must have minimum 8 characters.");
        hasError = true;
    } else if (/[0-9]/.test(password.value) === false || /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/.test(password.value) === false) {
        setErrorMessage(password, "Password must have a number and a special character.");
        hasError = true;
    } else {
        setSuccessMessage(password);
    }

    if (confirmPassword.value === "") {
        setErrorMessage(confirmPassword, "Password field can't be blank.");
        hasError = true;
    } else if (confirmPassword.value !== password.value) {
        setErrorMessage(confirmPassword, "Password did not match.");
        hasError = true;
    } else {
        setSuccessMessage(confirmPassword);
    }

    if (tosBox.checked) {
        setSuccessMessage(tosBox);
    } else {
        setErrorMessage(tosBox, "Please check the box");
        hasError = true;
    }

    return !hasError;
}

//Form eventlistener

form.addEventListener("submit", (event) => {
    event.preventDefault();

    loader.style.display = "flex";

    if (validatingSignUpForm()) {

        addUser();

        loader.style.display = "none";

        const headingTag = document.createElement('h4');
        headingTag.innerText = 'Sign Up done Successfully';

        const message = document.createElement('p');
        message.innerText = 'Redirecting to Home in 3 seconds...';

        const responseMessage = document.querySelector('.responseMessage');
        responseMessage.style.display = 'flex';
        responseMessage.style.color = '#088178';
        form.style.display = "none";

        const signupText = document.querySelector('h2');
        signupText.style.display = "none";

        responseMessage.appendChild(headingTag);
        responseMessage.appendChild(message);

        setTimeout(() => {
            document.location.href = "/";
        }, 3 * 1000);

    } else {
        // Show issue on Sign Up page
        loader.style.display = "none";
        console.log("Failed to signup");
    }
});


// Menu Bar

const bar = document.getElementById('bar');
const navbar = document.getElementById('navbar');

if (bar) {
    bar.addEventListener('click', () => {
        if (navbar.classList.contains('active')) {
            navbar.classList.remove('active');
        } else {
            navbar.classList.add('active');
        }
    });
} else {
    navbar.style.display = flex;
}